-- Cria as tabelas do banco de dados de exemplo
create table cargo (
    cod_cargo numeric(8) not null,
    descricao varchar(50) not null,
    primary key (cod_cargo)
);

create table departamento (
	cod_depto numeric(8) not null,
    descricao varchar(50) not null,
    primary key (cod_depto)
);

create table funcionario (
    matricula numeric(8) not null,
    nome varchar(100) not null,
    cod_cargo numeric(8) not null,
    cod_depto numeric(8) not null,
    salario numeric(10,2) not null,
    data_admissao timestamp not null,
    primary key (matricula),
    foreign key (cod_cargo) references cargo(cod_cargo),
    foreign key (cod_depto) references departamento(cod_depto)
);

