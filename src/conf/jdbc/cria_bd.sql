-- cria o usuário java
create user 'java'@'localhost' identified by 'java!';

-- cria o banco de dados
create database java;

-- dá permissão total ao usuário java a partir da máquina local
grant all on java.* to 'java'@'localhost';
