-- Insere dados no banco de dados de exemplo
insert into cargo values (1, 'Diretor');
insert into cargo values (2, 'Gerente');
insert into cargo values (3, 'Vendedor');
insert into cargo values (4, 'Estagiário');
insert into cargo values (5, 'Presidente');

insert into departamento values (1, 'Informática');
insert into departamento values (2, 'Recursos Humanos');
insert into departamento values (3, 'Vendas');
insert into departamento values (4, 'Suporte Técnico');
insert into departamento values (5, 'Administração');

insert into funcionario values (1, 'Marcelo Odebrecht', 5, 5, 200000.00, '1981-08-25');
insert into funcionario values (2, 'Joesley Batista', 5, 5, 100000.00, '1973-11-03');
insert into funcionario values (3, 'Wesley Batista', 1, 5, 50000.00, '1973-11-03');
insert into funcionario values (4, 'Rodrigo Rocha Loures', 2, 5, 5000.00, '1991-01-22');
insert into funcionario values (5, 'Eduardo Cunha', 3, 5, 1000.00, '2002-05-10');
