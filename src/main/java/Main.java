
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Linguagem Java
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Main {
    private static final String URL = "jdbc:mysql://localhost:3306/java?" +
            "serverTimezone=America/Sao_Paulo&useSSL=false";
    private static final String USUARIO = "java";
    private static final String SENHA = "java!";
    
    public static void main(String[] args) throws SQLException {
        Connection conn = DriverManager.getConnection(URL, USUARIO, SENHA);
        Statement stm = conn.createStatement();
        ResultSet rs = stm.executeQuery("select * from funcionario");
        
        System.out.println(String.format("%-1s %-100s %-10s",
                "MATRICULA", "NOME", "ADMISSÃO"));
        while (rs.next()) {
            System.out.println(String.format("%08d  %-100s %3$td/%3$tm/%3$tY",
                rs.getLong("matricula"), rs.getString("nome"), rs.getDate("data_admissao")));
        }
    }
}
